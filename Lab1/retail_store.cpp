/*
* Program : Make application for retail store
* Author  : Abhishek Shah
* Date    : 15 january,2017
*/

#include<iostream>

using namespace std;

class Customer
{
	private :
		string name;
		string address;
		long int custID;
		long int contactNo;
		

	public :
		void displayDetails();
		void enterCustomers();
		int searchCustomer(long int);
};

void Customer :: displayDetails(){													// Display Perticular customer's details
	cout << "Customer ID : " << custID << endl;
	cout << "Name: " << name << endl;
	cout << "Address: " << address << endl;
	cout << "Contact No: " << contactNo << endl;
}

void Customer :: enterCustomers(){													// Take customer details from the user
	cout << "Enter customer ID :";
	cin >> custID;
	cout << "Enter name : ";
	cin >> name;
	cout << "Enter Address : ";
	cin >> address;
	cout << "Enter Contact No: ";
	cin >> contactNo; 
}

int Customer :: searchCustomer(long int key){										// Search whether customer is exist or not
	if(key == custID)
		return 1;
	else
		return 0;
}

class Product
{
	private :
		int productID;
		string productName;
		
	public :
		void enterProducts();
		void displayProduct(long int);
		int searchProduct(long int);
		void displayProductList();
		long int productPrice;

};

void Product :: enterProducts(){													// Take product info from user
	cout << "Enter Product ID: ";
	cin >> productID;
	cout << "Enter Name: ";
	cin >> productName;
	cout << "Enter Price: ";
	cin >> productPrice;
}

void Product :: displayProduct(long int id){										// Display perticular Product Details
	cout << "Product ID: " << productID << endl;
	cout << "Name: " << productName << endl;
	cout << "Price: " << productPrice << endl;
}

int Product :: searchProduct(long int key){											// Search whether product is avail or not
	if(key == productID)
		return 1;
	else
		return 0;
}

void Product :: displayProductList()												// for Display whole list of Products
{
	cout << productID << " " << productName << endl;
}

class ProductList
{
	private : 
		long int productID;

	public :
		int quantity;
		void addProduct(long int);
		void removeProduct(long int);
	friend class Product;
};

void ProductList :: addProduct(long int key){										// Add product into the cart

 	this -> productID = key;
	cout << "Enter Quantity: " << endl;
	cin >> quantity;
}

void ProductList :: removeProduct(long int key){
			
	quantity = 0;	
}

int main(){

	int choice,i;
	short int noCust,noProd;
	char ch;
	long int key,total;
	Customer cust[100];
	Product prod[100];
	ProductList *productList=NULL;

	while(9){
	
		cout << "\n\n1. Enter Customer Details" << endl;							// Menu
		cout << "2. Enter Product Details" << endl;
		cout << "3. View Customer Details" << endl;
		cout << "4. View Product Details" << endl;
		cout << "5. Add Items to the cart" << endl;
		cout << "6. Remove Item from the cart" << endl;
		cout << "7. Invoice" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter your choice : " << endl;
		cin >> choice;		

		switch(choice){
		
		case 1:																	// Enter Customer Details
	
			i=0;	
				cout << "***** Enter every customer details *****" << endl;
			while(1){
				cust[i].enterCustomers();
				cout << "Would you like to Add more Customer Details (yes for 'y' and no for 'n') ?" <<endl;
				cin >> ch;
				if(ch == 'y' || ch == 'Y'){
				i++;
				continue;
				}
				else
					break;
			}
			noCust = i;
			break;
		
		case 2:																	// Enter Product Details
	
			i=0;
			cout << "***** Enter Every Product Details *****" << endl;
			while(1){
				prod[i].enterProducts();
				cout << "Would you like to Add more Product Details (yes for 'y' and no for 'n') ?" <<endl;
				cin >> ch;
				if(ch == 'y' || ch == 'Y'){
					i++;
					continue;
				}
				else
					break;
			}
			noProd = i;				
			break;

		case 3:																	// View Customer Details

			cout << "Enter customer ID: " << endl;
			cin >> key;
			
			for(i=0;i<=noCust;i++){												// For Customer Varification
				if(cust[i].searchCustomer(key))
					cust[i].displayDetails();
			}
			if(i==noCust+1)
				cout << "Please Enter valid customer ID" << endl;
			break;

		case 4:																	// View Product Details
	
			cout << "Enter product ID: " << endl;
			cin >> key;

			for(i=0;i<=noProd;i++){												// Product Varification
				if(prod[i].searchProduct(key))
					prod[i].displayProduct(key);
			}

			if(i==noProd)
				cout << "Product ID is not valid. Please Enter valid product ID" << endl;
			break;

		case 5:																	// Add into cart
		
			cout << "Product List " << endl;

			productList = (ProductList *) malloc(sizeof(ProductList)*noProd);
			
			while(1){
	
				for(i=0;i<=noProd;i++){											// To display List of Products
					prod[i].displayProductList();
				}

				cout << "Please add Item into the cart" << endl;
				cout << "Select ProductID: " << endl;							// Ask user which product he/she want
				cin >> key;
				
			    for(i=0; i<noProd; i++){										// Product Varification
			    	if(prod[i].searchProduct(key)){
			    		productList[i].addProduct(key);
			    		break;
		    		}
			    }

			    if(i==noProd)
			    	cout << "Please enter Valid ID" << endl;
			    
			    cout << "Would you like to add more items into the cart??" << endl;
			    cin >> ch;
			    
			    if(ch == 'y' || ch == 'Y')
			    	continue;
			    else
			    	break;
			}
			
			break;

		case 6:																	// Remove into cart

			cout << "Enter ProductID of which you want to remove: " << endl;
			cin >> key;

		    for(i=0; i<noProd; i++){										// Product Varification
		    	if(prod[i].searchProduct(key)){
		    		productList[i].removeProduct(key);
		    		break;
	    		}
		    }


			break;

		case 7:																	// Invoice
			
			cout << "********** INVOICE **********" << endl;
			for(i=0,total=0; i<noProd; i++){
				if(productList[i].quantity > 0){
					total = total + productList[i].quantity * prod[i].productPrice ;
				}
			}
			cout << "Final Amount: " << total << endl;
			break;

		case 0:																	// Exit
			return 0;
		}
	}



	cout << "End of the program" << endl;
	return 0;
}