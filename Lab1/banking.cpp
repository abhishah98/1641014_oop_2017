/*
* Program : Make a program to improve banking
* Author  : Abhishek Shah
* Date    : 10 January,2017
*/
#include <iostream>

using namespace std;

class Customer{

	private :
		string name;
		string address;
		long int ac_no;
		bool ac_type;															// 1 for saving and 0 for current
		long int balence;
				
	public :
		void enterDetails();
		void viewDetails();
		int searchAcount(long int);
		void addMoney(long int);
		void withdrawMoney(long int);				
};

void Customer :: enterDetails()													// Enter Details of customers
{
	int type;
	
	cout << "Enter name : ";
	cin >> name;	
	cout << "Enter address : ";
	cin >> address;
	cout << "Enter acount No : ";
	cin >> ac_no;
	
	do{
		cout << "Enter Acount Type ( 1 for Savings and 0 for Current ) : ";	
		cin >> type;
	
		if(type==1 || type==0){
			ac_type = type;
			type = 0;
		}
		
	}while(type);
	
	cout << "Enter Acount Ballence : ";
	cin >> balence;	
}
void Customer :: viewDetails()													// To view the details A/c no vise
{
	cout << " Name : " << name << endl;
	cout << " Address : " << address << endl;
	cout << " A/C No : " << ac_no << endl;
	if(ac_type)
		cout << "A/c Type : Savings" << endl;
	else
		cout << "A/C Type : Current" << endl;
	cout << " A/C Ballence : " << balence << endl;
}
int Customer :: searchAcount(long int keyAcount){								// Check the acount
	if(keyAcount == ac_no )
		return 1;
	else
		return 0;
}

void Customer :: addMoney(long int money){										// For Add money into acount
		balence += money;
		cout << "Transection Sucessfull" << endl;
		cout << "New balence : " << balence << endl;
}
void Customer :: withdrawMoney(long int money){									// Withdraw money from acount

	if(balence < money){
		cout << " You don't have enough money to withdraw !! " << endl;
	}
	else{
		balence -= money;
		cout << " Transection sucessfull ." << endl;
		cout << " New balence : " << balence << endl;
	}

}


int main(){
	int choice,i,noCust,type;
	long int acNo,money;

	Customer cust[100];

	while(9){
				
		cout << "\n\n\n1. Enter Customer Details" << endl;						// Menu 
		cout << "2. View Customer Details" << endl;
		cout << "3. Edit Customer Details" << endl;
		cout << "4. Withdraw Money for customer" << endl;						// Debit 
		cout << "5. Add Money for customer" << endl;							// Credit
		cout << "0. Exit" << endl;
		cout << "Enter your choice : " << endl;
		cin >> choice;

		switch(choice){

			case 1: 															// Enter details
				i=0,choice=1;
				while(choice){
					cust[i].enterDetails();
					i++;
					do{															// Asking for anathor customer
						cout << "Would you like to add more customer detail ( 1 for yes 0 for no ) ? "<< endl;
						cin >> type;
	
						if(type==1 || type==0){
							choice = type;
							type = 0;
						}
					}while(type);
				}
				noCust = i;
				break;

			case 2: 															// View details
				cout << "Enter acount number : " << endl;	
				cin >> acNo;
				for(i=0;i<noCust;i++){	
					if(cust[i].searchAcount(acNo)){
						cust[i].viewDetails();
						break;
					}
					else
						continue;
				}
				if(i==noCust)
					cout << " Please Enter valid A/c Number " << endl;
				break;

			case 3:																// Edit details
				cout << "Enter acount number : " << endl;	
				cin >> acNo;
				cout << "Re- Enter the details" << endl;
				for(i=0;i<noCust;i++){	
					if(cust[i].searchAcount(acNo)){
						cust[i].enterDetails();
						break;
					}
					else
						continue;
				}
				if(i==noCust)
					cout << " Please Enter valid A/c Number " << endl;
				break;

			case 4: 															// Withdraw Money
				cout << "Please enter the acount number : " << endl;
				cin >> acNo;
				for(i=0;i<noCust;i++){	
					if(cust[i].searchAcount(acNo)){
						cout << " Enter How many Rupees you Want to Withdraw ?? " <<endl;
						cin >> money;
						cust[i].withdrawMoney(money);
						break;
					}
					else
						continue;
				}
				if(i==noCust)
					cout << " Please Enter valid A/c Number " << endl;
				break;
				
								
			case 5: 															// Add Money
				cout << "Please enter the acount number : " << endl;
				cin >> acNo;
				for(i=0;i<noCust;i++){	
					if(cust[i].searchAcount(acNo)){
						cout << " Enter How many Rupees you Want to Add ?? " <<endl;
						cin >> money;
						cust[i].addMoney(money);
						break;
					}
					else
						continue;
				}				
				if(i==noCust)
					cout << " Please Enter valid A/c Number " << endl;
				break;

			case 0: 															// Exit
					return 0;
			}
		}
	return 0;
}	